function suma(num1,num2){
    return num1+num2
}

function tipos_de_datos(){

    var s = 'Hola';
    var n = 42;
    var b = true;
    var u

    console.log(typeof(s));
    console.log(typeof(n));
    console.log(typeof(b));
    console.log(typeof(u));

}

function tablaMultiplicar(tabla, hasta){
    for(i=1; i<=hasta; i++){
        console.log(tabla, 'x', i, ' = ', tabla*i);
    }
}

function tablaMultiplicar(tabla, hasta=10){
    for(i=1; i<=hasta; i++){
        console.log(tabla, 'x', i, ' = ', tabla*i);
    }
}

function sumar(a,b){
    return(a+b);
    console.log('Suma realizada');
}

function errores(){
    try{
        funcion_que_no_existe();
    }catch(ex){
        console.log("Error detectado: " + ex.description);
    }
    console.log("continua funcion despues del error");
}

function bucles(){
    console.log('Antes: ',p);
    for(let p=0;p<3;p++){
        console.log('- ',p);
    }

    console.log('Despues: ',p)
}

function constantes(){
    const NAME = 'valor fijo'
    console.log(NAME)
    NAME='cambio de valor'
}

function strings(){
    var texto1 = 'Texto 1'
    var texto2 = 'Texto 2'

    var texto3 = new String('Texto 3')
    var texto4 = new String('Texto 4')

    console.log(typeof(texto1))
    console.log(typeof(texto2))
    console.log(typeof(texto3))
    console.log(typeof(texto4))


}

function metodos_string(){
    var cadena1 = "Hola Mundo"

    var cadena2 = " I like JavaScript "

    var cadena3 = "Parangaricutirimicuaro"

    var caracter = cadena1.charAt(1);

    var usoTrim = cadena2.trim()

    var subcadena = cadena3.substr(2,10)

    console.log(usoTrim);

    console.log(caracter);

    console.log(subcadena)

    
}

function arreglos(){
    var array = new Array ('a','b','c');

    var array2 = ['a','b','c']
    var empty = []
    var mixto = ['a',5,true]

    console.log(array)
    console.log(array2)
    console.log(empty)
    console.log(mixto)
}

function metodosArray(){
    var array = ['a','b','c']

    array.push('d')

    console.log(array);

    array.unshift('z');

    array.shift()

    console.log(array)

    var array2=['a','b','c','d','e']

    var slizing=array2.slice(2,4);
    
    console.log(slizing)

    console.log(array2)

    array2.splice(2,2)

    console.log(array2)

}

function ordenamiento(){
    
    var array = [1,8,2,32,9,7,4]

    array.sort();

    console.log(array)

    return array

}



var f = function(a,b){
    if(a>b){
        return 1;
    }else if(a<b){
        return -1
    }
    return 0
}







